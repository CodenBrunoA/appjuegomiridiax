var app = {
	inicio: function() {
		DIAMETRO_BOLA = 50;
		DIAMETRO_HOYO = 200;
		alto = document.documentElement.clientHeight;
		ancho = document.documentElement.clientWidth;
		velocidadY = 0;
		velocidadX = 0;
		app.vigilaSensores();
		app.iniciaJuego();
		var timer;
		puntuacion = 0;
	},
	checkOverlap: function(bola, black) {
    	var boundsA = bola.getBounds();
    	boundsA.width = boundsA.width / 3.5;
    	boundsA.height = boundsA.height / 3.5;
    	var boundsB = black.getBounds();
    	boundsB.width = boundsB.width / 3.5;
    	boundsB.height = boundsB.height / 3.5;    	
   		return Phaser.Rectangle.intersects(boundsA, boundsB);
   	},
	
    	
	iniciaJuego: function() {
		function preload() {
			game.physics.startSystem(Phaser.Physics.ARCADE);
			game.stage.backgroundColor = '#7ec0ee';
			game.load.image('bola' , 'assets/bola.png');
			game.load.image('black' , 'assets/black.png');
			
			//document.querySelector("#puntaje").innerHTML = puntuacion;

			timer = game.time.create(false);
			timer.loop(20000,finalScore, this);
			timer.start();
		}

		function finalScore() {
			document.querySelector("body").innerHTML = "</br></br></br><h1 class='texto'>Puntuacion Final</br>"+ puntuacion+ " mundos descubiertos!!</h1> </br></br><h3 class='texto'>Agite para recomenzar</h3>" ;
			puntuacion = 0;
		}

		function create() {
			bola = game.add.sprite(app.inicioX() , app.inicioY() , 'bola');
			bola.width = 50;
			bola.height = 50;
			game.physics.arcade.enable(bola);
			bola.body.bounce.set(1);
			bola.body.collideWorldBounds = true;

			black = game.add.sprite(app.inicioXHoyo() , app.inicioYHoyo() , 'black');
			black.width = 100;
			black.height = 100;
		}
		
		function update() {
			bola.body.velocity.y = (velocidadY * 300);
			bola.body.velocity.x = (velocidadX * -300);
			//agregado para la colision

			if (app.checkOverlap(bola, black)){
				bola.destroy();
				black.destroy();
				create();
			    game.stage.backgroundColor = app.changeBackGround();
			    puntuacion++;
			    //document.querySelector("#puntaje").innerHTML = puntuacion;
					
			}
			
		}

		var estados = {preload: preload, create: create, update: update};
		var game = new Phaser.Game(ancho , alto , Phaser.CANVAS , 'phaser' , estados);
	},

	inicioX: function() {
		return app.numeroAleatorioHasta(ancho - DIAMETRO_BOLA);
	},

	changeBackGround: function() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
				 color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	},

	inicioY: function() {
		return app.numeroAleatorioHasta(alto - DIAMETRO_BOLA);
	},

	inicioXHoyo: function() {
		posicionXHoyo = app.numeroAleatorioHasta(ancho - DIAMETRO_HOYO/5);
		return posicionXHoyo;
	},

	inicioYHoyo: function() {
		posicionYHoyo = app.numeroAleatorioHasta(ancho - DIAMETRO_HOYO/5);
		return posicionYHoyo;
	},

	numeroAleatorioHasta: function(limite) {
		return Math.floor(Math.random() * limite);
	},

	vigilaSensores: function() {
		function onError() {
			console.log('onError');
		}

		function onSuccess(datosAceleracion) {
			app.detectaAgitacion(datosAceleracion);
			app.registraDireccion(datosAceleracion);
		}
		navigator.accelerometer.watchAcceleration(onSuccess, onError, {frequency: 5});
	},

	detectaAgitacion: function(datosAceleracion) {
		agitacionX = datosAceleracion.x > 38;
		agitacionY = datosAceleracion.y > 38;
	
		if(agitacionX || agitacionY) {
			app.recomienza();
		}
	},

	recomienza: function () {
		document.location.reload(true);
		puntuacion = 0;
	},

	registraDireccion: function(datosAceleracion) {
		velocidadX = datosAceleracion.x;
		velocidadY = datosAceleracion.y;
	},
};

if ('addEventListener' in document) {
	document.addEventListener('deviceready', function() {
		app.inicio();
	}, false);
}